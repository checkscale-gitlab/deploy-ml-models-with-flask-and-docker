import argparse
import json

from src.model import CustomModel
from src.transform import Transform


def main(train_path, test_path, save_model_path,
         test_size, n_estimators, max_depth, params=None):
    """
    Main script serves to load data, perform feature selection,
    transform categorical variables,  then finally train and save
    a decision tree classifier.
    :param train_path str: File path to training dataset.
    :param test_path str: File path to test dataset.
    :param save_model_path str: Desired file path where model will be saved.
    :param test_size float: Train/Test split size.
    :param n_estimators int: Number of estimators passed to decision tree.
    :param max_depth int: Max depth of decision tree.
    :param params dict: Optional user-provided dict of model params.
                        Default: None.
    :return pickle object: Returns saved pickled model
    """
    trns = Transform()
    raw_train, raw_test = trns.load_data(train_path, test_path)
    raw, raw_y, len_train, len_test = trns.feature_selection(raw_train,
                                                             raw_test)
    raw_transformed = trns.categorical_transformation(raw)
    X_train, X_test, y_train, y_test = trns.split(raw_transformed,
                                                  raw_y,
                                                  len_train,
                                                  test_size=test_size,
                                                  random_state=42)
    estimator = CustomModel(n_estimators=n_estimators,
                            max_depth=max_depth,
                            params=params)

    estimator_fitted = estimator.fit(X_train, y_train)

    estimator.save_model(estimator_fitted, save_model_path)
    print(f"\nModel saved in the following location: {save_model_path}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("train_path", type=str,
                        help="File path to training dataset.", required=True)
    parser.add_argument("test_path", type=str,
                        help="File path to test dataset.", required=True)
    parser.add_argument("save_model_path", type=str,
                        help="File path to random forest classifier model.",
                        required=True)
    parser.add_argument("test_size", type=int,
                        help="Train/Test split size.", default=0.3)
    parser.add_argument("n_estimators", type=int,
                        help="Number of estimators passed to decision tree.",
                        default=170)
    parser.add_argument("max_depth", type=int,
                        help="Max depth for decision tree.", default=12)
    parser.add_argument("params", type=json.loads,
                        help="Optional: Dict of model parameters to set.")
    args = parser.parse_args()

    main(**vars(args))
