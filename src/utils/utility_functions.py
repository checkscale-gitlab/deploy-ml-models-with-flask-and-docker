import datetime
import time

import numpy as np


def remove_dollar(x):
    """
    Remove $ from dollar amount features
    """
    try:
        x = str(x)
        return float(x.strip('$').replace(',', ''))
    except Exception:
        return np.nan


def per_float(x):
    """
    Converts the variable format from percentage to float
    """
    try:
        x = str(x)
        return float(x.strip('%')) / 100
    except Exception:
        return np.nan


def toYears(x):
    """
    Converts time from string to float (years since 1900-01-01)
    """
    try:
        x = datetime.datetime.strptime(x, "%b-%Y")
        x = x - datetime.datetime(1900, 1, 1)
        return x.days / 365.0
    except Exception:
        try:
            x = datetime.datetime.strptime(x, "%b-%y")
            if (x - datetime.datetime(2017, 12, 31)).days > 0:
                x = x - datetime.datetime(2000, 1, 2)
                return x.days / 365.0
            x = x - datetime.datetime(1900, 1, 1)
            return x.days / 365.0
        except ValueError:
            return np.nan


def timer(func):
    """
    This function is a decorator and prints runtime of specified function
    """

    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        if run_time <= 180:
            print(
                f"{func.__name__!r} finished. Runtime: {run_time:.4f}")
        else:
            print(
                f"{func.__name__!r} finished but could be optimized. Runtime: {run_time:.4f}")
        return value

    return wrapper_timer
